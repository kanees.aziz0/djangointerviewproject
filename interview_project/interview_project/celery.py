##http://docs.celeryproject.org/en/latest/django/first-steps-with-django.html
#


import os
#
from celery import Celery
# set the default Django settings module for the 'celery' program.
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'interview_project.settings')

from django.conf import settings  # noqa
app = Celery('interview_project')

# Using a string here means the worker will not have to
# pickle the object when using Windows.
app.config_from_object('django.conf:settings')

app.conf.beat_schedule = {
    'update_task_status': {
        'task': 'interview_app.tasks.update_task_status',
        'schedule': settings.UPDATE_STATUS_TIMINGS,
    },
}

app.autodiscover_tasks(lambda: settings.INSTALLED_APPS)

@app.task(bind=True)
def debug_task(self):
    print(('Request: {0!r}'.format(self.request)))
