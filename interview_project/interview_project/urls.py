from django.conf.urls import include, url

rest_framework = [url(r'^api-auth/', include('rest_framework.urls'))]
interview_app = [url(r'^interview_app/', include('interview_app.urls'))]

urlpatterns = [
    url(r'^', include((rest_framework,'rest_framework'), namespace='rest_framework')),
    url(r'^', include((interview_app,'interview_app'), namespace='interview_app')),
]