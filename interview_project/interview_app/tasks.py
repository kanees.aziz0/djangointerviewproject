#######################################################################
#
# tasks.py
#
# handles app tasks
#
#
#######################################################################
import ast
import time
from celery import shared_task
from django.conf import settings
from .models import Task
from .views_cluster.utils import (request_server, get_jitter_time)

update_status_timings = settings.UPDATE_STATUS_TIMINGS.split(' ')

@shared_task
def update_task_status():
    """
    Update task status for insuccessful api hits
    """
    task_objects=Task.objects.filter(status=0)
    for item in task_objects:
        if item.retries> settings.MAX_RETRIES_FOR_RES:
            item.status=2
            item.save()
        else:
            get_resource_for_task.delay(item.id)



@shared_task(name='get_resource_for_task')
def get_resource_for_task(id):
    task_object=Task.objects.get(id=id)
    # Sleep as per jitter time & retries
    time.sleep(get_jitter_time(task_object.retries))
    try:
        data = request_server(url='https://dog.ceo/api/breeds/image/random/{}'.format(str(task_object.random_res_no)), )
        data = ast.literal_eval(data.content.decode("UTF-8"))
    except Exception:
        data = None
    if data:
        # In case of successful hit change status of the task/resource
        task_object.status = 1
        task_object.resource = data['message'][0]
    else:
        task_object.retries += 1
    task_object.save()
