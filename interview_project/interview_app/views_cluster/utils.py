import string
import random
import requests
from .. import constants
from django.conf import settings

def generate_random_title(N=7):
    return ''.join(random.choices(string.ascii_uppercase + string.digits, k=N))

def request_server(url,
                   method=constants.HTTP_METHOD_GET,
                   params=None, headers=None):
    '''request server
    url: url of server/resource
    method: GET/POST
    params: Parameters
    headers: Additional headers
    '''
    if method == constants.HTTP_METHOD_GET:
        return requests.get(url, data=params, headers=headers)
    elif method == constants.HTTP_METHOD_POST:
        return requests.post(url, json=params, headers=headers)

def get_jitter_time(retry_attempt):
    temp= min(settings.MAXIMUM_WAIT_TIME, settings.WAIT_FOR_FIRST_RETRY*(pow(2,retry_attempt )))
    return (temp/2 + random.randint(0, temp/2 ))

