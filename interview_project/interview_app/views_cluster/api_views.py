import ast
import random
from django.conf import settings
from rest_framework import status
from django.views.decorators.cache import cache_page
from django.utils.decorators import method_decorator
from rest_framework.response import Response
from rest_framework.views import APIView
from .utils import (generate_random_title, request_server)
from .. models import (Task)



class DogsView(APIView):
    @method_decorator(cache_page(settings.CACHEVIEW['DOGS_VIEW']))
    def get(self, request, format=None):
        try:
            random_resource_no=random.randint(0,3)
            task_object=Task.objects.create(status=0, title=generate_random_title(), random_res_no=random_resource_no)
            try:
                data=request_server(url='https://dog.ceo/api/breeds/image/random/{}'.format(str(random_resource_no)), )
                data=ast.literal_eval(data.content.decode("UTF-8"))
            except Exception:
                data=None
            if data:
                # In case of successful hit change status of the task/resource
                task_object.status=1
                task_object.resource=data['message'][0]
                content = {
                    'status': 1,
                    'response_code': status.HTTP_200_OK,
                    'data':data
                }
            else:
                content = {'status': 0, 'response_code': status.HTTP_204_NO_CONTENT}
                task_object.retries += 1
            task_object.save()
        except Exception as e:
            content = {'status': 0, 'response_code': status.HTTP_500_INTERNAL_SERVER_ERROR}
        return Response(content)
