from django.db import models
from . import constants
# Create your models here.

class Task(models.Model):
    title = models.CharField(max_length=250, blank=False, null=False)
    date_created = models.DateTimeField(auto_now_add=True)
    date_updated = models.DateTimeField(auto_now=True)
    status = models.IntegerField(default=0)
    random_res_no= models.IntegerField(default=1)
    resource = models.TextField(blank=True, null=True)
    retries = models.IntegerField(default=0)

    def save(self, *args, **kwargs):
        if not self.id:
            self.date_created = constants.FORMATTED_TIME()
        self.date_updated = constants.FORMATTED_TIME()
        super(Task, self).save(*args, **kwargs)
