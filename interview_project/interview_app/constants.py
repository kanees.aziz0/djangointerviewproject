import datetime
import pytz

def DATE_TIME():
    return datetime.datetime.now(pytz.timezone('Asia/Calcutta'))
def FORMATTED_TIME():
    return datetime.datetime.strptime(str(DATE_TIME()).split('.')[0], '%Y-%m-%d %H:%M:%S')

MISSING_PARAM = 'Missing parameters'
GENERAL_EXCEPTION = 'Something went wrong'
HTTP_METHOD_POST = 'POST'
HTTP_METHOD_GET = 'GET'
