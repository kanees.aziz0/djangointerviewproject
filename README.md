Description:
* The Project is api based which queries a third party api source which may or may not provide data.
* The "DogsView" api is cached to avoid hitting the third party api service multiple times within a short time frame.
* The "Task" model keeps track whether data was provided & retries for each resource.
* The "get_jitter_time" function introduces jitter times for randomness preventing very short sleeps, keeping some of the slow down from the backoff(res: https://aws.amazon.com/blogs/architecture/exponential-backoff-and-jitter/).
* The celery/redis architecture runs a periodic task for getting resources of failed requests/tasks. The failed tasks are called using input from jitter algorithm asynchronously with indiidual wait times.
* The cache system should take care of thundering herd problem at application level. To optimize further we should look at network level solutions (Load balancer, Sharding by userId/ company name, active-archived data models)

Steps to run project:
1. Create virtual environment : virtualenv venv -p python3
2. Activate: source venv/bin/activate
3. i)  Install redis:"https://redis.io/" 
   ii) Install requirements: pip install -r requirements.txt
4. Makemigrations: change directory and inside project run i)python3 manage.py makemigrations ii) python3 manage.py migrate
5. Run server: python manage.py runserver
6. i)Make a request in browser/postman: "http://127.0.0.1:8000/interview_app/dogs_view/"
   ii) Make above request with internet connection off.
7. Run celery (change celery tasks times in settings.py for changing periodic tasks frequency):
    i)  celery -A interview_project worker -l info
    ii) celery -A interview_project beat -l info
    The beat will try to make requests for failed requests (status!=1 in Task model or 2nd case in 6th point).



